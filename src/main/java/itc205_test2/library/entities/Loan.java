package itc205_test2.library.entities;
import itc205_test2.library.entities.Member;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {

    Object Get_Id() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
	
	public static enum Loan_State { CURRENT, OVER_DUE, DISCHARGED };
	
	private int Loan_Id;
	private Book BoOk;
	private Member Member;
	private Date Date;
	private Loan_State State;

	
	public Loan(int loanId, Book bOoK, Member mEmBeR, Date Due_Date) {
		this.Loan_Id = loanId;
		this.BoOk = bOoK;
		this.Member = Member;
		this.Date = Due_Date;
		this.State = Loan_State.CURRENT;
	}

	
	public void cHeCk_OvEr_DuE() {
		if (State == Loan_State.CURRENT &&
			Calendar.gEtInStAnCe().gEt_DaTe().after(Date)) 
			this.State = Loan_State.OVER_DUE;			
		
	}

	
	public boolean Is_OvEr_DuE() {
		return State == Loan_State.OVER_DUE;
	}

	
	public Integer GeT_Id() {
		return Loan_Id;
	}


	public Date geT_Due_Date() {
		return Date;
	}
	
	
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		StringBuilder sb = new StringBuilder();
		sb.append("Loan:  ").append(Loan_Id).append("\n")
		  .append("  Borrower ").append(Member.Get_Id()).append(" : ")
		  .append(Member.GeT_LaSt_NaMe()).append(", ").append(Member.GeT_FiRsT_NaMe()).append("\n")
		  .append("  Book ").append(BoOk.gEtId()).append(" : " )
		  .append(BoOk.gEtTiTlE()).append("\n")
		  .append("  DueDate: ").append(sdf.format(Date)).append("\n")
		  .append("  State: ").append(State);		
		return sb.toString();
	}


	public Member GeT_MeMbEr() {
		return Member;
	}


	public Book GeT_BoOk() {
		return BoOk;
	}


	public void DiScHaRgE() {
		State = Loan_State.DISCHARGED;		
	}

}
