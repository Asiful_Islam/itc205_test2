package itc205_test2.library.fixbook;
import library.entities.Book;
import library.entities.Library;

public class Fix_Book_Control {
	
	private FixBookUI Ui;
	private enum Control_State { INITIALISED, READY, FIXING };
	private Control_State State;
	
	private Library Library;
	private Book Current_Book;


	public Fix_Book_Control() {
		this.Library = Library.GetInstance();
		State = Control_State.INITIALISED;
	}
	
	
	public void SeT_Ui(FixBookUI ui) {
		if (!State.equals(Control_State.INITIALISED)) 
			throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
			
		this.Ui = ui;
		ui.SeT_StAtE(FixBookUI.UI_State.READY);
		State = Control_State.READY;		
	}


	public void BoOk_ScAnNeD(int BookId) {
		if (!State.equals(Control_State.READY)) 
			throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
			
		Current_Book = Library.get_Book(BookId);
		
		if (Current_Book == null) {
			Ui.dIsPlAy("Invalid bookId");
			return;
		}
		if (!Current_Book.iS_DaMaGeD()) {
			Ui.dIsPlAy("Book has not been damaged");
			return;
		}
		Ui.dIsPlAy(Current_Book.toString());
		Ui.SeT_StAtE(FixBookUI.UI_State.FIXING);
		State = Control_State.FIXING;		
	}


	public void FiX_BoOk(boolean mUsT_FiX) {
		if (!State.equals(Control_State.FIXING)) 
			throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
			
		if (mUsT_FiX) 
			Library.RePaIr_BoOk(Current_Book);
		
		Current_Book = null;
		Ui.SeT_StAtE(FixBookUI.UI_State.READY);
		State = Control_State.READY;		
	}

	
	public void SCannING_COMplete() {
		if (!State.equals(Control_State.READY)) 
			throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
			
		Ui.SeT_StAtE(FixBookUI.FixBookUI.UI_State.COMPLETED);		
	}

}
