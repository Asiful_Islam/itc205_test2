package itc205_test2.library.payfine;
import library.entities.Library;
import itc205_test2.library.entities.Member;

public class Pay_Fine_Control {
	
	private PayFineUI UI;
	private enum Control_State { INITIALISED, READY, PAYING, COMPLETED, CANCELLED };
	private Control_State State;
	
	private Library Library;
	private Member Member;


	public Pay_Fine_Control() {
		this.Library = Library.Get_Instance();
		State = Control_State.INITIALISED;
	}
	
	
	public void Set_UI(PayFineUI UI) {
		if (!State.equals(Control_State.INITIALISED)) {
			throw new RuntimeException("PayFineControl: cannot call setUI except in INITIALISED state");
		}	
		this.UI = UI;
		UI.Set_State(PayFineUI.UI_State.READY);
		State = Control_State.READY;		
	}


	public void Card_Swiped(int Member_Id) {
		if (!State.equals(Control_State.READY)) 
			throw new RuntimeException("PayFineControl: cannot call cardSwiped except in READY state");
			
		Member = Library.get_Member(Member_Id);
		
		if (Member == null) {
			UI.Display("Invalid Member Id");
			return;
		}
		UI.Display(Member.toString());
		UI.Set_State(PayFineUI.UI_State.PAYING);
		State = Control_State.PAYING;
	}
	
	
	public void Cancel() {
		UI.Set_State(PayFineUI.UI_State.CANCELLED);
		State = Control_State.CANCELLED;
	}


	public double Pay_Fine(double Amount) {
		if (!State.equals(Control_State.PAYING)) 
			throw new RuntimeException("PayFineControl: cannot call payFine except in PAYING state");
			
		double Change = Member.Pay_Fine(Amount);
		if (Change > 0) 
			UI.Display(String.format("Change: $%.2f", Change));
		
		UI.Display(Member.toString());
		UI.Set_State(PayFineUI.UI_State.COMPLETED);
		State = Control_State.COMPLETED;
		return Change;
	}
	


}
